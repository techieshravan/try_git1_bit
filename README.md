try_git1_bit


Run some sand through your hands
Now is you chance to play around with this sample tutorial repository. Since this is a sample repository, you can't do anything wrong. Try both tasks below. How you perform each task depends on whether you are using Sourcetree or another Git client.
TASK 1. Clone the repository to your local machine
Cloning makes a local copy of the repository for you.
If you are using Sourcetree:
On Bitbucket, click the SSH/HTTPS button.
The system selects the URL for you.
Copy the URL.
Open Sourcetree.
Click the Add Repository button.
Paste the URL in the Source Path/URL field.
Place your cursor in the Destination Path field.
The system automatically completes the path for you.
Verify the path is what you want.
Press the Clone button.
Sourcetree clones your repository from Bitbucket to your local machine.
If you are using a terminal's (Linux/OSX/GitBash) command line:
Click the Clone button in Bitbucket.
Make sure the protocol is set to HTTPS.
The system selects the command for you.
Copy the command.
Open a terminal on your local machine.
Navigate to the directory where you want your files.
Paste the command at the prompt.
Press ENTER on your keyboard.
Git clones your repository from Bitbucket to your local machine.
TASK 2. Make a change
Make a change in the sample.html source file and push the change back to Bitbucket.
If you are using Sourcetree:
Double-click on your tutorial repo.
Select the Working Copy from the left hand navigation.
Select Show All from the dropdown.
Sourcetree shows the files in the working tree.
Select the sample.html file.
Right click and choose Open Terminal.
Using your favorite editor, edit the sample.html file.
Change the heading from My First File to Playing in the Sand.
Save and close the file.
Return to Sourcetree.
Sourcetree changes the file's icon to modified.
Choose Stage File.
Choose Commit.
Enter a commit message.
Press Commit.
Press Push to send your changes to Bitbucket.
After the push completes, use the Commits tab on Bitbucket to view your change.
If you are using a terminal's (Linux/OSX/GitBash) command line:
Go to your terminal window and navigate to the repository root.
Using your favorite editor, edit the sample.html file.
Change the heading from My First File to Playing in the Sand.
Save and close the file.
Stage the file with Git.
git add sample.html
Commit the change.
git commit -m "changing sample.html"
Push to Bitbucket.
git push
The system prompts you for a username/password.
Enter your Bitbucket accountname and the password.
After the push completes, use the Commits tab on Bitbucket to view your change.
